#!/bin/bash

clear

show_main_menu() {
while true; do
clear
echo "#################################################################"
if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

echo "#################################################################"
sudo apk update && sudo apk upgrade
echo "#################################################################"
sudo apk add zip iasl unzip
echo "#################################################################"
sudo pacman -Syu -y
echo "#################################################################"
sudo pacman -S zip iasl unzip -y
echo "#################################################################"
sudo pacman -Sc
echo "#################################################################"
sudo xbps-install -Syu -y
echo "#################################################################"
sudo xbps-install -S zip acpica-utils unzip -y
echo "#################################################################"
sudo apt-get update -y
echo "#################################################################"
sudo apt-get upgrade -y
echo "#################################################################"
sudo apt-get install --no-install-recommends zip acpica-tools unzip -y
echo "#################################################################"
sudo apt-get autoclean -y
echo "#################################################################"
sudo apt-get autoremove -y
echo "#################################################################"

clear

echo "#################################################################"
echo "(1)> (Compile) >> (OpenCore, for UEFI, with the modification using the name opencore-boot-loader)"
echo "(2)> (Exit)"
echo "#################################################################"

read -p "(Enter your choice) >> " choice
echo "#################################################################"

case $choice in
1)
show_OpenCore-for-uefi
;;
2)
exit 0
;;
*)
echo "(Invalid choice. Please try again)"
echo "#################################################################"
sleep 2
;;
esac
done
}

show_OpenCore-for-uefi() {
while true; do
clear

mkdir OpenCore-RELEASE
cd OpenCore-RELEASE
wget https://github.com/acidanthera/OpenCorePkg/releases/download/1.0.1/OpenCore-1.0.1-RELEASE.zip
unzip OpenCore-1.0.1-RELEASE.zip
rm -rf OpenCore-1.0.1-RELEASE.zip
cd ..
mv OpenCore-RELEASE source/OpenCore-RELEASE
sudo rm -rf opencore-boot-loader-for-uefi-version*.zip
sudo rm -rf temp
sudo mkdir temp
sudo mkdir temp/EFI
sudo cp -r source/OpenCore-RELEASE/X64/EFI/OC temp/EFI/
sudo cp -r source/OpenCore-Config/config.plist temp/EFI/OC/
iasl source/OpenCore-Config/MaLd0n.dsl
sudo cp -r source/OpenCore-Config/MaLd0n.aml temp/EFI/OC/ACPI
sudo zip -r opencore-boot-loader-for-uefi-version-0-0-3.zip temp
sudo rm -rf BOOTX64.efi
sudo rm -rf CLOVERX64.efi
sudo rm -rf boot6
sudo rm -rf boot7

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Compilation successful)"
echo "#################################################################"
echo "(Just extract the .zip with the name opencore-boot-loader-for-uefi-version-X-X-X)"
echo "#################################################################"
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi

break
done

echo "#################################################################"
}

show_main_menu
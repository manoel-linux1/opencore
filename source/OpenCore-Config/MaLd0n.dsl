DefinitionBlock ("", "SSDT", 1, "Linux", "Nice", 0x00001000)
{
    External (_SB_.PCI0.SBRG, DeviceObj)

    Scope (\_SB)
    {
        Device (EC)
        {
            Name (_HID, "ACID0001")
            Method (_STA, 0, NotSerialized)
            {
                If (_OSI ("Linux"))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }
        }

        Scope (\_SB)
        {
            Name (_HID, EisaId ("PNP0A05") )
            Name (_UID, "SD28301")
            }

        Device (USBL)
        {
            Name (_ADR, One)
            Method (_STA, 0, NotSerialized)
            {
                If (_OSI ("Linux"))
                {
                    Return (0x0F)
                }
                Else
                {
                    Return (Zero)
                }
            }
        }

        Method (MYL, 1, NotSerialized)
        {
            If (_OSI ("Linux"))
            {
                Return (One)
            }
            Else
            {
                Return (Zero)
            }
        }
    }
}
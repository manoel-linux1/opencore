# opencore

## FS

- It is only compatible with ext4/xfs: using other file systems may not work correctly.

## Binary

- The binary is available at: https://gitlab.com/manoel-linux1/opencore/-/releases

## Step to compile

- git clone https://gitlab.com/manoel-linux1/opencore.git

- cd opencore

- chmod a+x `build.sh`

- `./build.sh`